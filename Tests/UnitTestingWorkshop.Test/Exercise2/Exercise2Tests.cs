﻿using NUnit.Framework;
using UnitTestingWorkshop.BusinessLogic.Services;

namespace UnitTestingWorkshop.Test.Exercise2
{
    [TestFixture]
    public class Exercise2Tests
    {
        ForumMemberService _forumMemberService;

        [SetUp]
        public void Setup()
        {
            _forumMemberService = new ForumMemberService();
        }



        [Test]
        public void Get_Member_Has_Result()
        {
            var result = _forumMemberService.GetMemberById(123);

            Assert.IsNotNull(result);
        }



        [Test]
        public void Get_Member_Returns_Null_When_Member_Does_Not_Exist()
        {
            // TODO: finish me!

            //var result = _forumMemberService.GetMemberById(123);

            //   Assert.IsNull(result);
        }

    }
}
